document.addEventListener('DOMContentLoaded', function() {
    fetch('http://localhost:8000/number-of-rabbits/')
    .then(response => response.json())
    .then(data => {
        if (data.success) {
            const numberOfRabbits = data['count'];
            for (let i = 0; i < numberOfRabbits; i++) {
                addRabbitToScreen(i + 1);
            }
        }
    })
    .catch(error => console.error('Error:', error));
});

document.getElementById('addRabbit').addEventListener('click', function() {
    fetch('http://localhost:8000/add-rabbit/', { method: 'POST' })
    .then(response => response.json())
    .then(data => {
        if (data.success && document.querySelectorAll('.rabbit').length < 100) {
	        const totalRabbits = document.querySelectorAll('.rabbit').length;
            addRabbitToScreen(totalRabbits + 1);
        }
    })
    .catch(error => console.error('Error:', error));
});

document.getElementById('deleteRabbit').addEventListener('click', function() {
    fetch('http://localhost:8000/delete-rabbit/', { method: 'POST' })
    .then(response => response.json())
    .then(data => {
        if (data.success) {
            let rabbits = document.querySelectorAll('.rabbit');
            if (rabbits.length > 0) {
                rabbits[rabbits.length - 1].remove();
            }
        }
    })
    .catch(error => console.error('Error:', error));
});

function addRabbitToScreen(number) {
    let rabbit = document.createElement('div');
    rabbit.className = 'rabbit';
    rabbit.style.top = Math.random() * (document.getElementById('rabbit-container').clientHeight - 50) + 'px';
    rabbit.style.left = Math.random() * (document.getElementById('rabbit-container').clientWidth - 50) + 'px';

    // Добавляем номер кролика
    let label = document.createElement('span');
    label.className = 'rabbit-number';
    label.textContent = number;
    rabbit.appendChild(label);

    document.getElementById('rabbit-container').appendChild(rabbit);
}

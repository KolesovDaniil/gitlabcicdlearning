FROM --platform=arm64 python:3.12-slim-bullseye as base

WORKDIR /app
COPY . .
RUN pip install --no-cache-dir -r requirements.txt
WORKDIR /app/rabbits_app

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]
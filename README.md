    docker run --name some-postgres -e POSTGRES_PASSWORD=mysecretpassword -v postgres-data:/var/lib/postgresql/data -p 5432:5432 -d postgres

<br>

    docker run -e DB_NAME=testdb -e DB_USERNAME=postgres -e DB_PASSWORD=mysecretpassword -e DB_HOST=host.docker.internal -e DB_PORT=5432 -d -p 8000:80 test-app

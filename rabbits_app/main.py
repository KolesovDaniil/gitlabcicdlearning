import os

import psycopg2

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*']
)

connection_params = {
    "dbname": os.environ["DB_NAME"],
    "user": os.environ["DB_USERNAME"],
    "password": os.environ["DB_PASSWORD"],
    "host": os.environ["DB_HOST"],
    "port": os.environ["DB_PORT"]
}


@app.get("/number-of-rabbits/")
async def number_of_rabbits():
    with psycopg2.connect(**connection_params) as conn:
        with conn.cursor() as cur:
            cur.execute("SELECT COUNT(*) FROM rabbits")
            return {"success": True, "count": cur.fetchone()[0]}


@app.post("/add-rabbit/")
async def add_rabbit():
    with psycopg2.connect(**connection_params) as conn:
        with conn.cursor() as cur:
            cur.execute("INSERT INTO rabbits DEFAULT VALUES")
            conn.commit()
            cur.execute("SELECT COUNT(*) FROM rabbits")
            return {"success": True, "count": cur.fetchone()[0]}


@app.post("/delete-rabbit/")
async def delete_rabbit():
    with psycopg2.connect(**connection_params) as conn:
        with conn.cursor() as cur:
            cur.execute("DELETE FROM rabbits WHERE id in (SELECT MAX(id) FROM rabbits);")
            conn.commit()
            cur.execute("SELECT COUNT(*) FROM rabbits")
            return {"success": True, "count": cur.fetchone()[0]}
